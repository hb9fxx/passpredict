import numpy as np
from skyfield.api import load
from spytnik import traces
import unittest


class TestTrace(unittest.TestCase):
    """
    Test Trace
    """

    ts = load.timescale()

    def test_trace(self):
        t = traces.Trace(
            np.array(
                [
                    self.ts.ut1_jd(500000.2 + 2400000.5),
                    self.ts.ut1_jd(500000.1 + 2400000.5),
                    self.ts.ut1_jd(500000.3 + 2400000.5),
                    self.ts.ut1_jd(500000.4 + 2400000.5),
                ]
            ),
            np.array([12, 11, 10, 13]),
            np.array([4, 7, 5, 6]),
            111,
        )

        self.assertEqual(t.num_points, 4)
        self.assertEqual(t.site, 111)
        self.assertEqual(t.rsite, None)
        self.assertEqual(t.start_time, self.ts.ut1_jd(500000.1 + 2400000.5))
        self.assertEqual(t.end_time, self.ts.ut1_jd(500000.4 + 2400000.5))
        np.testing.assert_array_equal(
            t.timestamps,
            np.array(
                [
                    self.ts.ut1_jd(500000.1 + 2400000.5),
                    self.ts.ut1_jd(500000.2 + 2400000.5),
                    self.ts.ut1_jd(500000.3 + 2400000.5),
                    self.ts.ut1_jd(500000.4 + 2400000.5),
                ]
            ),
        )
        np.testing.assert_array_equal(t.frequencies, np.array([11, 12, 10, 13]))
        np.testing.assert_array_equal(t.fluxes, np.array([7, 4, 5, 6]))

    def test_load_trace_group(self):
        tg = traces.TraceGroup("test", 1111)

        tg.load_dat_file("tests/data/test.dat")

        self.assertEqual(tg.name, "test")
        self.assertEqual(tg.satid, 1111)
        self.assertEqual(tg.num_traces, 5)
        self.assertEqual(tg.num_points, 461)
        self.assertEqual(tg.start_time.ut1, 2459414.842470)
        self.assertEqual(tg.end_time.ut1, 2459440.919362)

        self.assertEqual(tg.traces[0].site, 9876)
        self.assertEqual(tg.traces[0].rsite, None)
        self.assertEqual(tg.traces[0].num_points, 41)
        self.assertEqual(tg.traces[0].start_time.ut1, 2459414.842470)
        self.assertEqual(tg.traces[0].end_time.ut1, 2459414.842956)

        self.assertEqual(tg.traces[1].site, 9876)
        self.assertEqual(tg.traces[1].rsite, None)
        self.assertEqual(tg.traces[1].num_points, 87)
        self.assertEqual(tg.traces[1].start_time.ut1, 2459414.892968)
        self.assertEqual(tg.traces[1].end_time.ut1, 2459414.894021)

        self.assertEqual(tg.traces[2].site, 9876)
        self.assertEqual(tg.traces[2].rsite, 9997)
        self.assertEqual(tg.traces[2].num_points, 95)
        self.assertEqual(tg.traces[2].start_time.ut1, 2459414.894033)
        self.assertEqual(tg.traces[2].end_time.ut1, 2459414.895121)

        self.assertEqual(tg.traces[3].site, 9876)
        self.assertEqual(tg.traces[3].rsite, None)
        self.assertEqual(tg.traces[3].num_points, 213)
        self.assertEqual(tg.traces[3].start_time.ut1, 2459440.916630)
        self.assertEqual(tg.traces[3].end_time.ut1, 2459440.919072)

        self.assertEqual(tg.traces[4].site, 9876)
        self.assertEqual(tg.traces[4].rsite, 9998)
        self.assertEqual(tg.traces[4].num_points, 25)
        self.assertEqual(tg.traces[4].start_time.ut1, 2459440.919084)
        self.assertEqual(tg.traces[4].end_time.ut1, 2459440.919362)
